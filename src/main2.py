import sys
import json
import math
from pprint import pprint
from extrapackage.bayesiannet import *
from extrapackage.node import *

def loadJSON():
    with open('network.json') as file:
        data = json.load(file)
    return data

def main():
    str  = raw_input().split(" ")
    # m is the number of estimates
    M = int(str[0])
    # n is the number of times the calculation is repeated
    N = int(str[1])
    data = loadJSON()
    bayesian_net = BayesianNet(data)
    # Solving for P(c|s,w)
    X = 'C'
    e = ['S', 'W']

    probs = []
    for i in range(0, N):
        probs.append(bayesian_net.likelihood_weighting(X,e,M))
    mn = mean(probs)
    var = variance(probs,mn)

    print round(mn, 6), round(var, 6)

    return 1

# calculate the mean
def mean(data):
    mean = math.fsum(data)/float(len(data))
    return mean

# calculate the variance
def variance(data, mean):
    var = (math.fsum([math.pow(d-mean,2) for d in data])/float(len(data)-1))
    return var

if __name__ == "__main__":
    main()
