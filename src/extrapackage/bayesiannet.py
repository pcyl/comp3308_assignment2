'''P(c|s,w) = P(c)P(s|c)P(w|s)'''

'''
Bayesian Network
'''
from node import Node
import random

class BayesianNet():
    def __init__(self,network):
        self.nodes = []

        for node in network['nodes']:
            newNode = Node()
            newNode.id = node['id']
            newNode.parent = node['parent']
            newNode.probability = node['probability']
            self.nodes.append(newNode)

    def likelihood_weighting(self,X,e,M):
        # Holds samples
        W = {}

        for j in xrange(0,M):
            x,w = self.weighted_sample(e)
            for sample in W:
                if x in W[sample]['x']:
                    W[sample]['w'] += w
                else:
                    W.update({ 'x' : x, 'w' : w})

        # Normalize
        w_sum = 0.0
        w_dictionary = []
        for sample in W:
            if X in W[sample]['x']:
                w_sum += 1

        return float(w_sum)/float(M)

    def weighted_sample(self,e):
        # variable to hold the calculated weight of this sample
        w = 1.0
        # variable to hold the state of each node of this sample
        x = []

        for node in self.nodes:
            evidence, state = node.is_evidence(e)

            # check first if the node is evidence
            if evidence:
                # check the node for parents
                if node.has_parents():
                    w = w * node.get_probability(state, node.parents)
                else:
                    w = w * node.get_probability(state)

                # depending on the value of the node, append it to x
                if state:
                    x.append(node.id)
                else:
                    x.append(node.neg_id())
            else:
                if node.has_parents():
                    givenParents = []
                    for parent in node.parents:
                        if parent in x:
                            givenParents.append(parent)
                        else:
                            neg = "-" + parent
                            if neg in x:
                                givenParents.append(neg)
                    # P(x|parents(x))
                    probTrue = node.get_probability(True, givenParents)

                    # Random sample from P(X|parents(X))
                    random.seed()
                    randomNum = random.uniform(0,1.0)

                    if randomNum <= probTrue:
                        x.append(node.id)
                    else:
                        x.append(node.neg_id())
        return x,w