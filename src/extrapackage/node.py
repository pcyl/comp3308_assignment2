'''
Getting the characteristic of the bayesian network through
JSON
'''

class Node():
    def __init__(self):
        self.id = None
        self.parents = []
        self.probability = {}
#         self.priorProbability = None

    def get_probability(self,state,nodeParents):
        if not nodeParents:
            if not self.parents:
                if state is True:
                    return self.probability[0]['value']
                else:
                    return 1.0 - self.probability[0]['value']
            else:
                return None
        else:
            given = nodeParents[0]
            val = None
            for prob in self.probability:
                for id, condition in enumerate(given):
                    if val is None and id == 0:
                        val = prob.get(condition)
                    elif val is not None:
                        val = val.get(condition)
                if val is not None:
                    if state is False:
                        val = 1.0 - val
                    return val
            return val


    def has_parents(self):
        if self.parents is not None:
            return True
        else:
            return False

    def is_evidence(self,e):
        if self.id in e:
            return True, True
        else:
            if self.neg_id() in e:
                return True, False
            else:
                return False, None

    def neg_id(self):
        return "-" + self.id